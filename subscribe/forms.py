from django import forms

class SubscribeForm(forms.Form):
    name = forms.CharField(required = True)
    email = forms.EmailField(required = True)
    password = forms.CharField(required = True, widget = forms.PasswordInput())
    confirm_password = forms.CharField(required = True, widget = forms.PasswordInput())  

    def is_valid(self):
 
        valid = super(SubscribeForm, self).is_valid()
 
        # we're done now if not valid
        if not valid:
            return valid 

        if not (self.cleaned_data['password'] == self.cleaned_data['confirm_password']):
            self._errors['invalid_password'] = 'Password is invalid'
            return False         

        return True;            
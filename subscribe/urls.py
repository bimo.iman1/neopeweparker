from django.conf.urls import url
from django.urls import path

from . import views

app_name = "subscribe"

urlpatterns = [
    path('', views.show_subscribe, name='show_subscribe'),
    path('email_list', views.json_mail, name = 'email_list'),
    path('submit', views.submit_form, name = 'submit_form'),
]

from django.test import TestCase, Client, SimpleTestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve, reverse
from django.apps import apps
from .apps import SubscribeConfig
from .models import Subscriber
from .forms import SubscribeForm
from . import views

class AppNameTest(TestCase):
    def test_apps(self):
        self.assertEqual(SubscribeConfig.name, 'subscribe')
        self.assertEqual(apps.get_app_config('subscribe').name, 'subscribe')

class SubscribePageTest(TestCase):
    def test_show_using_right_show_funct(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, views.show_subscribe)
    
    def test_submit_using_right_funct(self):
        found = resolve('/subscribe/submit')
        self.assertEqual(found.func, views.submit_form)
    
    def test_subscribe_page_status_code(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)
    
    def test_submit_page_status_code(self):
        response = Client().get('/subscribe/submit')
        self.assertEqual(response.status_code,200)

    def test_subscribe_page_using_right_template(self):
        response = self.client.get(reverse('subscribe:show_subscribe'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'subscriber.html')

class FormTest(TestCase):
    def test_forms_valid(self):
        form_data = {'name': 'hueuheuheuhe', 'email':'hohohoh@hehe.com', 'password':'hoho', 'confirm_password':'hoho'}
        form = SubscribeForm(data=form_data)
        self.assertTrue(form.is_valid())
    
    def test_post_form(self):
        response = self.client.post('/subscribe/submit', {'name': 'hueuheuheuhe', 'email':'hohohoh@hehe.com', 'password':'hoho', 'confirm_password':'hoho'})
        self.assertEqual(Subscriber.objects.count(), 1)

    def test_forms_invalid_email(self):
        form_data = {'name': 'hueuheuheuhe', 'email':'hohohohhehe', 'password':'hoho', 'confirm_password':'hoho'}
        form = SubscribeForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_forms_invalid_password(self):
        form_data = {'name': 'hueuheuheuhe', 'email':'hohohoh@hehe.com', 'password':'hoho', 'confirm_password':'hehe'}
        form = SubscribeForm(data=form_data)
        self.assertFalse(form.is_valid())

class ModelTest(TestCase):
    def test_create_model(self):
        form_data = {'name': 'hueuheuheuhe', 'email':'hohohoh@hehe.com', 'password':'hoho', 'confirm_password':'hoho'}
        form = SubscribeForm(data=form_data)
        form.is_valid()
        subscriber = Subscriber()
        subscriber.name = form.cleaned_data['name']
        subscriber.save()
        self.assertEqual(subscriber.name, 'hueuheuheuhe')

    def test_models_str(self):
        Subscriber.objects.create(name= 'hueuheuheuhe', email='hohohoh@hehe.com', password='hoho')
        counting_all_available_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available_subscriber, 1)
        string=Subscriber.objects.get(email = 'hohohoh@hehe.com')
        self.assertEqual('hohohoh@hehe.com', str(string))

    
# Create your tests here.

from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import SubscribeForm
from .models import Subscriber

response = {}

def show_subscribe(request):
    form = SubscribeForm()
    response['form'] = form
    return render(request,'subscriber.html', response)

def json_mail(request):
    json_email = {'email':[]} 
    for subscriber in Subscriber.objects.all():
        json_email['email'].append(subscriber.email)
    return JsonResponse(data = json_email)

def submit_form(request):
    form = SubscribeForm(request.POST)  
    if (form.is_valid()):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        subscriber = Subscriber(name = response['name'],
                                email = response['email'],
        password = response['password'])
        subscriber.save()
        return JsonResponse({'success': True})
    return JsonResponse({'success': False})


# Create your views here.

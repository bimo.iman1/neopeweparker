from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MessagingForm
from .models import MessageSenderKeeper

response = {}

def index(request):
    form = MessagingForm()
    response['form'] = form
    messages = MessageSenderKeeper.objects.all()
    response['message']=messages
    return render(request,'landing.html', response)

def submit_message(request):
    form = MessagingForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['sender'] = request.POST['sender']
        response['message'] = request.POST['message']
        message = MessageSenderKeeper(
            sender = response['sender'],
            message = response['message'],
            )
        message.save()
        return HttpResponseRedirect('/landing/')
    else:
        return HttpResponseRedirect('/')

def redirecting(request):
    return HttpResponseRedirect('/landing/')

def showprofile(request):
    return render(request,'story1.html')
# Create your views here.
from django import forms

class MessagingForm(forms.Form):
    sender = forms.CharField(max_length=50)
    message = forms.CharField()                        
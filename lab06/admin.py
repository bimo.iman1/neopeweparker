from django.contrib import admin
from .models import MessageSenderKeeper

admin.site.register(MessageSenderKeeper)

# Register your models here.

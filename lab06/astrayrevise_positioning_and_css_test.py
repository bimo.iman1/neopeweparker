import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class FunctionalTest():

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable') 
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless') 
        chrome_options.add_argument('disable-gpu') 
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        
    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
    
    def test_functional_input_with_selenium(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)

        sender = selenium.find_element_by_name('sender')
        sender.send_keys('Bot')
        message = selenium.find_element_by_name('message')
        message.send_keys('coba-coba')
        submit_button = selenium.find_element_by_id('submit')
        submit_button.send_keys(Keys.RETURN)
        self.assertIn('coba-coba', self.selenium.page_source)
        

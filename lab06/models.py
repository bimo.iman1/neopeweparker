from django.db import models
from django.utils import timezone

class MessageSenderKeeper(models.Model):
    sender = models.CharField(max_length=50)
    time = models.DateTimeField(default = timezone.now)
    message = models.TextField()
    # Create your models here.

    def __str__(self):
        if (len(self.message)<=50):
            shorted_message = self.message
        else:
            shorted_message = self.message[:50]
        return self.sender + " - " + shorted_message

// Loading the loader icon
$(window).load(function() {
    $('#loading').hide();
 });

 var point = 0;
 function update(id){
    var buttonid = '#' + id
    var buttonobject= $(buttonid)
    var clear = '<i class="material-icons" onclick = update(' + id + ')>clear</i>' + 'Clear Favorite'
    var plus = '<i class="material-icons" onclick = update(' + id + ')>add_circle</i>' + 'Add to Favorite'
    console.log(buttonobject.html())
    if ((buttonobject).html().includes('add_circle')) {
        buttonobject.html(clear)
        point++;
    } else if ((buttonobject).html().includes('clear')) {
        buttonobject.html(plus)
        point--
    }
    $('#jumlah').html('Favorited : ' + point)
 }

//  When the web is ready 
$(document).ready( function() {
    $( function() {
        // Loading the accordion element
        $( "#accordion" ).accordion({
            collapsible: true,
            heightstyle: "content"
        });
    });

    $( function runAJAX() {
        $.ajax({
        url: '/library/api', 
        success: function(result) {
            var jsonobject = result.items
            for (var i= 0; i < jsonobject.length; i++){
                var book = jsonobject[i].volumeInfo
                var title = book['title']
                var desc = book['description']
                var pagecount = book['pageCount']
                var publisher = book['publisher']
                var publishedDate = book['publishedDate']
                var picture = '<img src=' + '"' + book['imageLinks'].thumbnail + '"' + '>'
                var htmlstring = 
                '<tr>' + 
                '<th scope="row">' + (i+1) + '</th>' + 
                '<td>' + picture + '</td>' + 
                '<td>' + pagecount + '</td>' + 
                '<td>' + title + '</td>' +
                '<td>' + desc + '</td>' +
                '<td id='+ (i) + '>' + '<i class="material-icons" onclick = "update(' + i + ')">add_circle</i>' +'Add to Favorite'+ '</td>'
                + '</tr>'
                $("#booktable").append(htmlstring)
            }
        }
        });
    });
    
    // Hovering element and flipping cover 
    $("#pageflip").hover(function() {
        $("#pageflip img , .msg_block").stop()
        .animate({width: "100px",height: "100px"}, 500);
    }, 
    function() {
        $("#pageflip img").stop()
        .animate({width: "50px",height: "52px"}, 220);
        
        $(".msg_block").stop()
        .animate({width: "50px",height: "50px"}, 200);
    });
    
    var change = 0;
    $("#change").click(function() {
        if (change == 1){
            normal();
            change = 0;
        } else {
            changed();
            change = 1;
        }
    })
    function normal() {
        $('body').animate({
            'background-color': 'teal'
        })
        $('.profile').animate({
            'color': 'black'
        })
    }

    function changed() {
        $('body').animate({
            'background-color': 'blue'
        })
        $('.profile').animate({
            'color':'white'
        })
    }

    $('#findbutton').click(function(){
        var value = document.getElementById('findbook').value
        $("#booktable").html("")
        $.ajax({
            url: '/library/api/' + value, 
            success: function(result) {
                var jsonobject = result.items
                if (jsonobject === undefined) {
                    $("tbody").append("Not Found")
                } else {
                    for (var i= 0; i < jsonobject.length; i++){
                        var book = jsonobject[i].volumeInfo
                        var title = book['title']
                        var desc = book['description']
                        var pagecount = book['pageCount']
                        var publisher = book['publisher']
                        var publishedDate = book['publishedDate']
                        var picture = '<img src=' + '"' + book['imageLinks'].thumbnail + '"' + '>'
                        var htmlstring = 
                        '<tr>' + 
                        '<th scope="row">' + (i+1) + '</th>' + 
                        '<td>' + picture + '</td>' + 
                        '<td>' + pagecount + '</td>' + 
                        '<td>' + title + '</td>' +
                        '<td>' + desc + '</td>' +
                        '<td id='+ (i) + '>' + '<i class="material-icons" onclick = "update(' + i + ')">add_circle</i>' +'Add to Favorite'+ '</td>'
                        + '</tr>'
                        $("#booktable").append(htmlstring)
                    }
                }
            },
            });
    })
    
    $('#dialog-message').hide()
    $('#formsubscribe').change(function() {
        // Redisable the button
        $('#submit').attr('class','btn disabled')
        // Normalize the popup
        $('#id_email').removeAttr("title")
        var email = document.getElementById('id_email').value
        var pass = document.getElementById('id_password').value
        var confirm = document.getElementById('id_confirm_password').value
        var name = document.getElementById('id_name').value
        $.ajax({
            url: '/subscribe/email_list',
            success: function(result) {
                // Checking available email
                var jsonobject = result.email
                for (var i = 0; i < jsonobject.length; i++){
                    if (jsonobject[i] == email){
                        $('#id_email').attr('title', 'Email is already taken')
                        var tooltips = $( "#id_email" ).tooltip({
                            position: {
                              my: "left top",
                              at: "right+5 top-5",
                              collision: "none"
                            }
                          }).tooltip("open");
                        return
                    }
                }
                // Checking password
                if (pass.length != 0 && pass != confirm) {
                    $('#id_confirm_password').attr('title', 'Password is not the same')
                        var tooltipspass = $( "#id_confirm_password" ).tooltip({
                            position: {
                              my: "left top",
                              at: "right+5 top-5",
                              collision: "none"
                            }
                          }).tooltip("open");
                        return
                }
                // Checking for not empty field
                if (name.length != 0 && pass.length != 0 && email.length != 0){
                    document.getElementById("submit").disabled = false;
                    $('#submit').attr('class','btn btn-primary')
                }

                //To submit without refreshing
                $('form').submit(function(event) {
                    var formData = $(this).serializeArray();
                    $.ajax( {
                        type : 'POST',
                        url : '/subscribe/submit',
                        data: formData,
                        dataType: 'json',
                    }).done(function(response){
                        $( "#dialog-message" ).dialog({
                            modal: true,
                            buttons: {
                              Ok: function() {
                                $( this ).dialog( "close" );
                              }
                            }
                          });
                          document.getElementById("submit").disabled = true;
                          $('#submit').attr('class','btn disabled')
                    }).fail(function(response){
                        alert('error')
                    });
                    event.preventDefault();
                })
            }
        })
    })
    // Start here for next func
} );
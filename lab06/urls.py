from django.conf.urls import url
from django.urls import path

from . import views

app_name = "lab06"

urlpatterns = [
    path('', views.redirecting),
    path('landing/', views.index, name='index'),
    path('process/', views.submit_message, name = 'form_process'),
    path('profile/', views.showprofile, name = 'profile'),
]

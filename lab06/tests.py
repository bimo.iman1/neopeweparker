from django.test import TestCase, Client, SimpleTestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve, reverse
from django.apps import apps
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from . import views
from .models import MessageSenderKeeper
from .forms import MessagingForm 
from .apps import Lab06Config

class AppTests(TestCase):
    def test_apps(self):
        self.assertEqual(Lab06Config.name, 'lab06')
        self.assertEqual(apps.get_app_config('lab06').name, 'lab06')

class LandingPageTests(TestCase):
    def test_landing_page_status_code(self):
        response = Client().get('/landing/')
        self.assertEquals(response.status_code,200)

    def test_landing_page_using_index_func(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, views.index)
    
    def test_have_landing_page_message(self):
        response = Client().get('/landing/')
        self.assertContains(response,'Hello')
    
    def test_landing_page_if_slash_empty(self):
        response = Client().get('/')
        found = resolve('/')
        self.assertEquals(response.status_code,302)
        self.assertEqual(found.func, views.redirecting)

    def test_view_url_by_index_name(self):
        response = self.client.get(reverse('lab06:index'))
        self.assertEquals(response.status_code, 200)
    
    def test_view_url_by_form_process_name(self):
        response = self.client.get(reverse('lab06:form_process'))
        self.assertEquals(response.status_code, 302)

    def test_landing_page_using_submit_message_func(self):
            found = resolve('/process/')
            self.assertEqual(found.func, views.submit_message)
    
    def test_post_message_content(self):
        response = self.client.post('/process/', {'sender': 'eminem', 'message':'rekt'})
        self.assertEqual(MessageSenderKeeper.objects.count(), 1)
        new_message = MessageSenderKeeper.objects.first()
        self.assertEqual(new_message.sender, 'eminem')

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('lab06:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'landing.html')

class ModelsTest(TestCase):
    def test_models_created(self):
        MessageSenderKeeper.objects.create(sender='Eminem', message='killshot')
        counting_all_available_message = MessageSenderKeeper.objects.all().count()
        self.assertEqual(counting_all_available_message, 1)
        eminem_message = MessageSenderKeeper.objects.get(sender='Eminem')
        self.assertEqual('Eminem - killshot',str(eminem_message))

class FormTest(TestCase):
    def test_forms_valid(self):
        form_data = {'sender': 'something', 'message':'Ngiehehehe'}
        form = MessagingForm(data=form_data)
        self.assertTrue(form.is_valid())
        message = MessageSenderKeeper()
        message.sender = form.cleaned_data['sender']
        message.save()
        self.assertEqual(message.sender, "something")
    
    def test_forms_invalid(self):
        form_data = {'sender': 'somethingheuheuhuehuehuehuehuehuehuehuehuehuehuehuehe', 
        'message':'Ngiehehehe'}
        form = MessagingForm(data=form_data)
        self.assertFalse(form.is_valid())

class ProfileTest(TestCase):
    def test_profile_page_status_code(self):
        response = Client().get('/profile/')
        self.assertEquals(response.status_code,200)

    def test_profile_page_using_showprofile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.showprofile)
    
    def test_have_landing_page_container(self):
        response = Client().get('/profile/')
        self.assertContains(response,'Bimo Iman S')
    
    def test_view_profile_uses_correct_template(self):
        response = self.client.get(reverse('lab06:profile'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story1.html')

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('--dns-prefetch-disable') 
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless') 
        chrome_options.add_argument('disable-gpu') 
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()
    
    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
    
    def test_functional_input_with_selenium(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)

        sender = selenium.find_element_by_name('sender')
        sender.send_keys('Bot')
        message = selenium.find_element_by_name('message')
        message.send_keys('coba-coba')
        submit_button = selenium.find_element_by_id('submit')
        submit_button.send_keys(Keys.RETURN)
        self.assertIn('coba-coba', self.selenium.page_source)
    
    def test_positioning_with_selenium_1(self):
        selenium = self.selenium
        selenium.get('http://astrayrevise.herokuapp.com/landing/')
        time.sleep(5)
        h1 = selenium.find_element_by_tag_name('h1')
        self.assertIn('Hello!',h1.get_attribute('innerHTML'))

    def test_positioning_with_selenium_2(self):
        selenium = self.selenium
        selenium.get('http://astrayrevise.herokuapp.com/landing/')
        time.sleep(5)
        result = selenium.find_element_by_name('Result')
        self.assertIn('Message:',result.get_attribute('innerHTML'))

    def test_css_outerProfileBorder_with_selenium(self):
        selenium = self.selenium
        selenium.get('http://astrayrevise.herokuapp.com/landing/')
        time.sleep(5)
        outerProfileBorder = selenium.find_element_by_id('resultsched')
        self.assertEquals('rgb(242, 188, 115)',outerProfileBorder.value_of_css_property('border-color'))    

    def test_css_profilePage_with_selenium(self):
        selenium = self.selenium
        selenium.get('http://astrayrevise.herokuapp.com/landing/')
        time.sleep(5)
        profilePage = selenium.find_element_by_id('profilePage')
        self.assertEquals('rgba(255, 186, 92, 1)',profilePage.value_of_css_property('background-color'))
    

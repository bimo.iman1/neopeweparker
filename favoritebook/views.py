from django.shortcuts import render
from django.http import JsonResponse
import requests
def show_library(request):
    return render(request, 'library.html')

def get_api(request, keyword = 'quilting'):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
    jsonobject = requests.get(url).json()
    return JsonResponse(jsonobject)
# Create your views here.

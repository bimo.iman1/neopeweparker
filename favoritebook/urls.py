from django.conf.urls import url
from django.urls import path

from . import views

app_name = "favoritebook"

urlpatterns = [
    path('show_library/', views.show_library, name='show_library'),
    path('api/', views.get_api, name = 'get_api'),
    path('api/<str:keyword>', views.get_api, name = 'get_api2'),
]
